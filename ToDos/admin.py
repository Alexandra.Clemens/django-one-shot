from django.contrib import admin

# Register your models here.
from todos.models import TodoList

class TodolistAdmin(admin.ModelAdmin):

admin.site.register(TodoList, TodolistAdmin)
